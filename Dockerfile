FROM python:3.7-alpine
COPY Pipfile /Pipfile
COPY Pipfile.lock /Pipfile.lock

RUN set -x \
 && apk add --no-cache ca-certificates ffmpeg py-crcmod python3 libxml2-dev python3-dev alpine-sdk libxslt-dev \
 && pip3 install --no-cache-dir pipenv \
 && pipenv --python /usr/bin/python3 lock -r > requirements.txt \
 && cat requirements.txt \
 && pip3 install --no-cache-dir -r requirements.txt \
 && pip3 uninstall -y pipenv \
 && wget https://storage.googleapis.com/pub/gsutil.tar.gz \
 && tar zxf gsutil.tar.gz
