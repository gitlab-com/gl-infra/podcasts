.PHONY: build-docker-ci
REGISTRY_ACCESS_USER ?= gl-infra
BASEDIR=$(CURDIR)
PUBLICDIR=$(BASEDIR)/public
PORT=8000

build-docker-ci:
	docker build -t registry.gitlab.com/gitlab-com/gl-infra/podcasts -f Dockerfile .

push-docker-ci: build-docker-ci
ifndef REGISTRY_ACCESS_TOKEN
	$(error REGISTRY_ACCESS_TOKEN is required to push to the gitlab registry)
endif
	@docker login -u ${REGISTRY_ACCESS_USER} -p ${REGISTRY_ACCESS_TOKEN} registry.gitlab.com
	docker push registry.gitlab.com/gitlab-com/gl-infra/podcasts

serve:
	cd $(PUBLICDIR); python -m SimpleHTTPServer ${PORT}
