#!/usr/bin/env python

import requests
import youtube_dl
from feedgen.feed import FeedGenerator
import os
import sys
from datetime import datetime
import urllib.parse
import json

# Channel ID for GitLab unfiltered
CHANNEL_ID = os.environ.get("UNFILTERED_CHANNEL_ID")
GCS_BUCKET = "gitlab-podcasts"
API_KEY = os.environ.get("YOUTUBE_API_KEY")
MAX_RESULTS = os.environ.get("MAX_RESULTS", "20")


class MyLogger(object):
    def debug(self, msg):
        pass

    def warning(self, msg):
        pass

    def error(self, msg):
        print(msg)


def my_hook(d):
    if d["status"] == "finished":
        print("Done downloading, now converting ...")


def fetch_videos(search_str):
    search_query = urllib.parse.quote(search_str)
    search_url = (
        f"https://www.googleapis.com/youtube/v3/search"
        f"?part=snippet&maxResults={MAX_RESULTS}"
        f"&channelId={CHANNEL_ID}"
        f"&order=date"
        f"&q={search_query}"
        f"&type=video&key={API_KEY}"
    )

    r = requests.get(search_url)

    if r.status_code != 200:
        print(f"Received {r.status_code} when fetching videos from youtube")
        sys.exit(1)

    return reversed(r.json()["items"])


def process_videos(search_str):

    fg = FeedGenerator()
    fg.load_extension("podcast")
    fg.podcast.itunes_category("Technology")
    fg.podcast.itunes_image(
        f"https://{GCS_BUCKET}.storage.googleapis.com/unfiltered.png"
    )
    fg.podcast.itunes_summary(f"GitLab {search_str}, unfiltered")
    fg.title(f"GitLab {search_str}")
    fg.link(href=f"https://www.youtube.com/channel/{CHANNEL_ID}", rel="alternate")

    fg.image(f"https://{GCS_BUCKET}.storage.googleapis.com/unfiltered.png")
    fg.description(f"GitLab {search_str}")

    feed_dirname = search_str.lower().replace(" ", "-")
    try:
        os.mkdir(f"feed-files/{feed_dirname}")
    except OSError as exc:
        if exc.errno != os.errno.EEXIST:
            raise
        pass
    # https://github.com/ytdl-org/youtube-dl/blob/master/youtube_dl/YoutubeDL.py#L128-L278
    ydl_opts = {
        "format": "bestaudio/best",
        "restrictfilenames": True,
        # Using the file archive causes problems with caching when
        # names change
        # "download_archive": "feed-files/file-archive.txt",
        "postprocessors": [
            {
                "key": "FFmpegExtractAudio",
                "preferredcodec": "mp3",
                "preferredquality": "192",
            }
        ],
        "logger": MyLogger(),
        "progress_hooks": [my_hook],
    }

    processed = []
    for video in fetch_videos(search_str):
        video_id = video["id"]["videoId"]
        title = video["snippet"]["title"]
        url = f"https://www.youtube.com/watch?v={video_id}"
        print(f"Downloading {title}")
        with youtube_dl.YoutubeDL(ydl_opts) as ydl:
            ydl_info = ydl.extract_info(url)
            fname = os.path.splitext(ydl.prepare_filename(ydl_info))[0] + ".mp3"
            title = ydl_info["title"]
            upload_date = datetime.strptime(ydl_info["upload_date"], "%Y%m%d").strftime(
                "%Y-%m-%d"
            )
            mp3_path = f"{feed_dirname}/{upload_date}-{fname}"
            dest_path = f"feed-files/{mp3_path}"
            if os.path.exists(fname):
                os.rename(fname, dest_path)
            fe = fg.add_entry()
            link = f"https://{GCS_BUCKET}.storage.googleapis.com/{mp3_path}"
            fe.id(link)
            fe.title(f"{upload_date} {title}")
            fe.description(title)
            fe.enclosure(link, 0, "audio/mpeg")
            print(f"Adding {title} to processed videos")
            processed.append({"upload_date": upload_date, "title": title, "link": link})

    fg.rss_str(pretty=True)
    fg.rss_file(f"feed-files/{feed_dirname}.xml")
    with open("feed-files/processed", "w") as f:
                    f.write(
                        json.dumps(
                            processed, sort_keys=True, indent=4, separators=(",", ": ")
                        )
                    )


if __name__ == "__main__":
    process_videos("Group Conversation")
